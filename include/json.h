#ifndef _JSON_H
#define _JSON_H

#include "jansson.h"
#include "dtsclient.h"
#include "time.h"
#include "locale.h"

/*
 *	Macros difinition
 */

/*
 *	Type declaration
 */


/*
 *	Data declaration
 */	
struct _tuple_ {
	int				index;
	char 			*kks;
	int 			(*mksig_func)(struct _case_ *, const char *, const char*);
	struct _tuple_ 	*next;
};

 struct _config_ {
 	struct _tuple_ *head;
 	struct _tuple_ *tail;
 };


/*
 *	Function declaration
 */
extern int parse_msg(const char*, DTSClient&, struct _tuple_ *);

#ifdef __cplusplus
extern "C" {
#endif
int 	load_cfg(char *path, struct _config_ *);
void 	create_kks(json_t *, struct _config_ *);
int 	calc_quality(int );
int 	calc_tm(json_t *ts, struct _time_ *tm);

#ifdef __cplusplus
}
#endif

#endif /* _NAME_H */
