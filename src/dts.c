#include <time.h>
#include <math.h>
#include "handlers.h"

/*
 *	Data definition:
 */
/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name        - load_an_sig

Usage       - parse_msg

Prototype in    - json.c   

Description - Формирует аналоговый сигнал для отправки в ДТС

*---------------------------------------------------------------------*/

int make_an(struct _case_ *an, const char *sig, const char *kks)
{
    float       val;
    int         rc=0;

    printf("make_an\n");
    val = atof(sig);
    if (finitef(val))
    {  
        makeAValue( &an->value, an->index, an->quality, val, an->ts.sec, an->ts.msec);
    }
    else
    {
        fprintf(stderr, "Value is not a finite number: %f\n", val);
        rc=-1;
    }

    return rc;
}
/*---------------------------------------------------------------------*

Name        - load_bin_sig

Usage       - parse_msg

Prototype in    - json.c   

Description - Формирует дискретный сигнал для отправка в DTS  

*---------------------------------------------------------------------*/
int make_bin(struct _case_ *bin, const char *sig, const char *kks)
{
    int             val;
    int             rc=0;
    int             len = strlen(sig);

    if (len>0)
    {
        val = atoi(&sig[len-1]);
        if (val != 0 && val !=1)
        {
            fprintf(stdout, "The value is not binary: %s\n", sig );
            rc=-1;
        }
        else
        {
            makeBValue( &bin->value, bin->index, bin->quality, val, bin->ts.sec, bin->ts.msec);
        }
    }
    else
    {
        fprintf(stderr, "The value's length is NULL\n");
        rc=-1;
    }    

    return rc;
}
/*---------------------------------------------------------------------*

Name        - send_signal   

Usage       - json.c

Prototype in    - dts.c

Description - Отправляет сформированный сигнал в dts   

*---------------------------------------------------------------------*/
int send_signal(json_t *msg, DTSClient& client, struct _tuple_ *tuple)
{
    int             rc=0;
    struct _case_   sig;
    const char      *value = json_string_value(json_object_get(msg, "value"));
    const char      *ks = json_string_value(json_object_get(msg,"kks"));

/*    if (fill_tm(json_object_get(msg, "ts"), &sig.tm) == 0)
    {*/ sig.index = tuple->index;
        sig.ts.sec = atoi(json_string_value(json_object_get(json_object_get(msg,"ts"), "sec")));
        sig.ts.msec = atoi(json_string_value(json_object_get(json_object_get(msg,"ts"), "ms")));
        sig.quality = get_quality(atoi(json_string_value(json_object_get(msg,"fault"))));
        if (tuple->mksig_func(&sig, value, ks) == 0)
        {
            switch( client.put( sig.value ) )
            { 
                case -1 :
                    printf( "Analog value put error:%i\n", client.getError() );
                    break; 

                case 1 :
                    printf( "Analog value put warning:%i\n", client.getError() );
                    break; 
            }
        }
        else
        {
            fprintf(stderr, "send_signal: The signal was not sent\n");
            rc= -1;
        }
/*    }
    else
    {
        fprintf(stderr, "The signal can not be sent because of the fill_tm error\n");
        rc=-1;
    }*/

    return rc;

}
/*---------------------------------------------------------------------*

Name		- rcvFunc	

Usage		- main.c

Prototype in	- dts.c

Description	- Отображает, поступающие в ДТС значения    

*---------------------------------------------------------------------*/

int rcvFunc(void *argPtr, Value &value, int32_t chnlId)
{
    char            *kks;

	switch(value.type)
	{
		case Ana_VT :
        {   
		    AData *dataPtr = NULL;
			value.getData(dataPtr);
/*            printf( "Receive CH[%i] ANA[%i]:%f(%i)\n", chnlId, value.idx, dataPtr->value, dataPtr->quality);*/
            kks = get_kks(out.head, &value.idx);
            if (strcmp(kks, "KKSWASNOTFOUND") != 0)
            {
                printf( "Receive msg {\"ts\":{\"sec\":\"%i\",\"ms\":\"%d\"},\"fault\":\"2\",\"value\":\"%f\",\"type\":\"event\",\"kks\":\"%s\"}\n", value.sec, value.msec,dataPtr->value, kks);
            }
            else
            {
               /* printf( "Receive CH[%i] ANA[%i]:%f(%i)\n", chnlId, value.idx, dataPtr->value, dataPtr->quality); */
            }
        }
			break;

		case Bin_VT :
        {
			BData *dataPtr = NULL;
			value.getData(dataPtr);
/*            printf( "Receive CH[%i] BIN[%i]:%i(%i)\n", chnlId, value.idx, dataPtr->value, dataPtr->quality);*/
            kks = get_kks(out.head, &value.idx);
            if ( strcmp(kks, "KKSWASNOTFOUND") != 0)
            {
                printf( "Receive msg {\"ts\":{\"sec\":\"%i\",\"ms\":\"%d\"},\"fault\":\"2\",\"value\":\"%d\",\"type\":\"event\",\"kks\":\"%s\"}\n", value.sec, value.msec,dataPtr->value, kks);
            }
            else
            {
                /*printf( "Receive CH[%i] BIN[%i]:%i(%i)\n", chnlId, value.idx, dataPtr->value, dataPtr->quality); */ 
            }
        }
			break;

		case Int_VT :
        {
			IData *dataPtr = NULL;
			value.getData(dataPtr);
			/*printf( "Receive CH[%i] INT[%i]:%i(%i)\n", chnlId, value.idx, dataPtr->value, dataPtr->quality);*/
        }
			 break;

		case Grp_VT :
        {
			GData *dataPtr = NULL;
			value.getData(dataPtr);
			/*printf( "Receive CH[%i] GRP[%i] (GT:%i GL:%u)\n", chnlId, value.idx, dataPtr->groupType, dataPtr->size);*/
        }
			break;

		default : break;
	}
	return 0;
}

/*struct _ts_ set_ts()                          //Не используется пока
{
    struct _ts_ sigtime;
    struct timeval  tv;
    struct tm       *loctime;

    gettimeofday(&tv,NULL);
    loctime=localtime((time_t *)&tv.tv_sec);
    strftime(sigtime.date,64,"%a %b %d %Y",loctime);
    strftime(sigtime.time,32,"%H:%M:%S",loctime);
    sigtime.msec = tv.tv_usec/1000;

    return sigtime;
}
*/