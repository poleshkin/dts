#include "handlers.h"
#include "getopt/opt.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>

struct _config_ in;
struct _config_ out;
char message[512];

int main(int argc, char *argv[])
{	
	int				fd,n;
	int 			rc=0;
	struct timeval 	tv;
	fd_set			fds;
	FILE			*data;

	if (getoption(option, argc, argv) != OPT_SUCCESS)
	{
		puthelp();
	}

	data=fopen(_GETOPTS(option, OPT_FIFO), "r");
	if (data == NULL)
	{
		perror("Couldn't open FIFO file: \n");
		rc = -1;
	}
	else
	{
		fd = fileno(data);
	}
	get_config(_GETOPTS(option, OPT_INCONF), &in);
	get_config(_GETOPTS(option, OPT_OUTCONF), &out);

	setlocale(LC_ALL, "ru_RU.UTF-8");
	DTSClient client(rcvFunc,NULL);

	if (rc==0)
	{
		while (1)
		{
			tv.tv_sec=0;
			tv.tv_usec=500000;
			FD_ZERO(&fds);
			FD_SET(fd,&fds);
			n=select(fd+1,&fds,NULL,NULL,&tv);

			if (n > 0)
			{
				while (fgets(message,512,data))
				{
					rc = parser(message,client, &in);
				}
			}
		}
	}	
	else
	{
		printf("Не удалось загрузить файл сигналов НУ\n");
	}

	return rc;
}
