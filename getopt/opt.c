
#include <stdio.h>

#include "opt.h"
#include "getopt/setopt.h"


/*
 *	Data definition:
 */

static char 	*help[] = {
	"Usage: \"PPD\" -in [PATH] -out [PATH] -f [PATH]\n\n",
	"-h\t--help		display this help and exit\n",
#if defined ( _USER_OPTION_ )
	"-in\t--inconf 	open the configuration file\n",
	"-out\t--outconf 	open the configuration file\n",
	"-f\t--fifo 		open the fifo file\n",	
#endif
	NULL };

#if defined ( _USER_OPTION_ )
char		inconf[128] = "/dev/null";
char		outconf[128] = "/dev/null";
char		fifo[128] = "/dev/null";


#endif

struct _option_	option[] = {
	{   1, "-h", "--help",	help,		 GETOPT_FLAG, (setopt_t) getmsg 	},
#if defined ( _USER_OPTION_ )
	{	2, "-in", "--inconf", 	inconf,	 GETOPT_MSG, (setopt_t) setstr 	},
	{	3, "-f", "--fifo", 		fifo,	 GETOPT_MSG, (setopt_t) setstr	},
	{	4, "-out", "--outconf", outconf, GETOPT_MSG, (setopt_t) setstr 	},			

#endif
	{ 0, NULL, NULL, NULL, 0, NULL }
};

/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
void puthelp(void)
{
	puthlp(help);
}

