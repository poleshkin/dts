#ifndef _DTS_H
#define _DTS_H


#include "dtsclient.h"
#include "jansson.h"
/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */



/*
 *	Data declaration
 */	


extern struct _config_ in;
extern struct _config_ out;

struct _fqual_ {
	int 			fault;
	int 			quality;
	struct _fqual_ 	*next;
};

struct _time_ {				//НЕ ИСПОЛЬЗУЕТСЯ В ТЕКУЩЕЙ ВЕРСИИ
	unsigned int sec;
	unsigned int msec;
};

struct _case_ {
	int 			index;
	Value 			value;
	int 			quality;
	struct _time_ 	ts;

};

struct _ts_ {					//НЕ ИСПОЛЬЗУЕТСЯ В ТЕКУЩЕЙ ВЕРСИИ
	char 		date[64];
	char 		time[32];
	short int 	msec;
};

/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif
int 		rcvFunc(void *argPtr, Value &value, int32_t chnlId);
int 		make_an(struct _case_ *, const char *, const char*);
int 		make_bin(struct _case_ *, const char *, const char*);
int 		send_signal(json_t *msg, DTSClient& c, struct _tuple_ *);
int 		get_quality(int);
struct _ts_ set_ts();

#ifdef __cplusplus
}
#endif

#endif /* _NAME_H */
