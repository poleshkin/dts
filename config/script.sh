#!/bin/bash

LANG=ru_RU.utf8

switch=0
while [ 1 ]
do
	if [[ "$switch" -eq 0 ]]; then
		# echo "{ \"ts\":{ \"date\":\"$(date +"%a %b %d %Y")\", \"time\":\"$(date +"%T")\", \"ms\":\"001\" }, \"fault\":\"2\", \"value\":\"0\", \"type\":\"event\", \"db\":{ \"nick\":\"Т ХЛД ГЦТ2\", \"name\":\"Температура холодной нитки в петле (2 канал)\", \"block\":\"ПАЭ-02\"}, \"kks\":\"41YAS16CT032A_XQ00\"}" >> fifo
		echo "{ \"ts\":{ \"sec\":\"$(date +"%s")\",\"ms\":\"$(expr $(date +%N) / 1000000)\"}, \"fault\":\"2\", \"value\":\"0\", \"type\":\"event\", \"db\":{ \"nick\":\"Т ХЛД ГЦТ2\", \"name\":\"Температура холодной нитки в петле (2 канал)\", \"block\":\"ПАЭ-02\"}, \"kks\":\"41YAS16CT032A_XQ00\"}" >> fifo
		switch=1
	else 
		# echo "{ \"ts\":{ \"date\":\"$(date +"%a %b %d %Y")\", \"time\":\"$(date +"%T")\", \"ms\":\"001\" }, \"fault\":\"2\", \"value\":\"1\", \"type\":\"event\", \"db\":{ \"nick\":\"Т ХЛД ГЦТ2\", \"name\":\"Температура холодной нитки в петле (2 канал)\", \"block\":\"ПАЭ-02\"}, \"kks\":\"41YAS16CT032A_XQ01\"}" >> fifo
		echo "{ \"ts\":{ \"sec\":\"$(date +"%s")\",\"ms\":\"$(expr $(date +%N) / 1000000)\"}, \"fault\":\"2\", \"value\":\"1\", \"type\":\"event\", \"db\":{ \"nick\":\"Т ХЛД ГЦТ2\", \"name\":\"Температура холодной нитки в петле (2 канал)\", \"block\":\"ПАЭ-02\"}, \"kks\":\"41YAS16CT032A_XQ00\"}" >> fifo
		switch=0
	fi
	sleep 1
done		