#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include "handlers.h"

json_t *root;
json_error_t    error;
/*
 *	Data definition:
 */      

/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		- load_cfg

Usage		- main.c

Prototype in	- json.c   

Description	- Разбирает сообщение str и заполняет полученными данными структуру msg    

*---------------------------------------------------------------------*/

int parse_msg(const char *msg, DTSClient& c, struct _tuple_ *node)
{
    int     rc=0;
    char    *kks;

	root=json_loads(msg, JSON_DECODE_ANY, &error);
	if (root) 
	{
		if (json_typeof(root)==JSON_OBJECT)
		{
            kks = (char *)(json_string_value(json_object_get(root,"kks")));
            while (node)
            {
                if (strcmp(kks,node->kks)==0)
                {
                    rc=send_signal(root, c, node);
                    break;
                }
                else
                {
                    node=node->next;
                }
            }
        }       
        else
        {
            fprintf(stderr, "Parse_msg: Msg's type is not a json object\n");
            rc=-1;
        }  
    }
    else
    {
    	fprintf(stderr, "Json_loads error %s\n", error.text);
        rc=-1;
    }

    return rc;
}

/*---------------------------------------------------------------------*

Name        - load_cfg

Usage       - main

Prototype in    - json.c   

Description - Загружает файл конфигурации с ККСами    

*---------------------------------------------------------------------*/

int load_cfg(char *path, struct _config_ *table)
{
    int             rc=0;
    json_t          *kks;
    size_t          arr,i;


    root = json_load_file(path, JSON_ENCODE_ANY, &error);

    if (root)
    {
        if (json_typeof(root)==JSON_OBJECT)
        {
            kks = json_object_get(root,"kks");
            if (json_typeof(kks)==JSON_ARRAY)
            {
                arr = json_array_size(kks);
                for (i = 0; i<arr; i++)
                {
                    create_kks(json_array_get(kks,i), table);
                }
            }
            else
            {
                fprintf(stderr, "Config error: The key 'kks' is not an array\n");
                rc = -1;
            }
        }
        else
        {
            fprintf(stderr, "Config error: The 'root' is not a JSON_OBJECT\n");
            rc = -1;
        }
    }
    else
    {
        fprintf(stderr, "json_load_file error: %s\n", error.text);
        rc = -1;
    }

    return rc;

}

/*---------------------------------------------------------------------*

Name        - load_kks

Usage       - load_cfg

Prototype in    - json.c   

Description - Создает ноду в списке ККСов

*---------------------------------------------------------------------*/
void create_kks(json_t *element, struct _config_ *table)
{   
    struct _tuple_ *newk;

    newk = (struct _tuple_ *) malloc(sizeof(struct _tuple_));

    if (newk == NULL)
    {
        printf("Unable to allocate memory for the new KKS node\n");
        exit(-1);
    }
    else
    {
        newk->index = json_integer_value(json_object_get(element, "index"));
        newk->kks = (char*)json_string_value(json_object_get(element, "kks"));
        if (strcmp(json_string_value(json_object_get(element, "type")), "an") == 0)         //"Свитчуем" тип ккса
        {
            newk->mksig_func = &make_an;
        }
        else if (strcmp(json_string_value(json_object_get(element, "type")), "bin") == 0)
        {
            newk->mksig_func = &make_bin;
        }
        else if (strcmp(json_string_value(json_object_get(element, "type")), "null") == 0)
        {
            newk->mksig_func = NULL;
        }
        else
        {
            fprintf(stderr, "The KKS has a wrong type\n");
            exit(-1);
        }

        if (!table->head)                //Подвязываем ноду к списку
        {
            table->head=newk;
            table->tail=newk;
        }
        else
        {
            table->tail->next = newk;
            table->tail = newk; 
        }   

        newk->next=NULL; 
    }
}

/*---------------------------------------------------------------------*

Name        - load_kks

Usage       - load_cfg

Prototype in    - json.c   

Description - Создает ноду в списке ККСов

*---------------------------------------------------------------------*/
int calc_tm(json_t *ts, struct _time_ *tm)
{
    static char date[64];
    int         rc=0;
    char        *dt, *hms;
    struct tm   utc;

    if (json_typeof(ts)==JSON_OBJECT)
    {
        dt = (char *)(json_string_value(json_object_get(ts,"date")));           //Перевод времени в UTC
        hms = (char *)(json_string_value(json_object_get(ts,"time")));
        sprintf(date, "%s %s",dt, hms);
        strptime(date, "%a %b %d %Y %H:%M:%S", &utc);
        tm->sec = (uint32_t)(mktime(&utc));
        tm->msec = atoi(json_string_value(json_object_get(ts, "ms")));

    }
    else
    {
        fprintf(stderr, "Fill_tm: Type of json var 'ts' is not an object\n");
        rc=-1;
    }

    return rc;
}
