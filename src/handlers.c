#include "handlers.h"

/*
 *	Data definition:
 */
struct _fqual_  fq[4]={
    {2, 0, &fq[1]},
    {3, 1, &fq[2]},
    {4, 1, &fq[3]},
    {5, 1, NULL},
 };
/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-	parser	

Usage		- main.c	

Prototype in	-     handler.c

Description	-     Обработчик разбора сообщения

*---------------------------------------------------------------------*/
int parser(const char *msg, DTSClient& c, struct _config_ *intable)
{
	int rc=0;
	rc = parse_msg(msg, c, intable->head);	

	return rc;
}
/*---------------------------------------------------------------------*

Name		-	init	

Usage		- main.c	

Prototype in	-     handler.c

Description	-     Обработчик загрузки файла конфигурации

*---------------------------------------------------------------------*/
void get_config(char *path, struct _config_ *table)
{
	if (load_cfg(path, table) != _SUCCESS)		//Загружаем файл конфигурации. 
	{
		fprintf(stderr, "Couldn't open config file\n");
		exit(-1);
	}
	else
	{
		printf("cfg was loaded\n");
	}

}
/*---------------------------------------------------------------------*

Name		-	get_index	

Usage		- main.c	

Prototype in	-     handlers.c

Description	-     НЕ ИСПОЛЬЗУЕТСЯ

*---------------------------------------------------------------------*/
/*static int search_index(void *key, struct _tuple_ *node)
{
	int rc = _FAILURE ;
	char *kks = ((char *)key);

        if (strcmp(kks,node->kks) == _SUCCESS)
        {
			rc = _SUCCESS;
        }
        
 	return rc;
}*/
/*---------------------------------------------------------------------*

Name		-	get_kks		

Usage		- main.c	

Prototype in	- handlers.c

Description	-    Проверяет соответствие ключа индексу и возвращает ккс при совпадении
*---------------------------------------------------------------------*/
static int search_kks(void *key, struct _tuple_ *node)
{
	int rc = _FAILURE ;
	int index = *((int *)key);

        if (index == node->index)
        {
 			rc = _SUCCESS;
        }

    return rc;
}

/*---------------------------------------------------------------------*

Name		-	get_data	

Usage		- main.c	

Prototype in	-     handlers.c

Description	-    Перебирает таблицу соответствий и вызывает обработчик проверки соответствия
*---------------------------------------------------------------------*/
static struct _tuple_* pass_table(struct _tuple_ *node, void *key, int (*get_tuple)(void *key, struct _tuple_ *node))
{
	while (node!=NULL && get_tuple(key,node)!=_SUCCESS)
	{
		node=node->next;
	}

	return node;
}
/*---------------------------------------------------------------------*

Name        - calc_quality

Usage       - make_bin make_an

Prototype in    - json.c   

Description - Возвращает из списка качество сигнала quality, соответствующее ошибке fault
*---------------------------------------------------------------------*/

int get_quality(int fault)
{
    struct _fqual_ *p = fq;
    int quality;

    while (p)
    {
        if (fault == p->fault)
        {
        quality = p->quality;
        break;
        }
        p=p->next;
    }

    return quality;
}

char* get_kks(struct _tuple_ *head, int *index)
{
	char           *kks = "KKSWASNOTFOUND";
    struct _tuple_ *p = head;
    struct _tuple_ *tuple = pass_table(p, (void *)index, &search_kks);

    if (tuple != NULL)
    {
    	kks = tuple->kks;
    }


    return kks;
    
}

//НЕ ИСПОЛЬЗУЕТСЯ, УДАЛЯТЬ ПОКА ЧТО ЖАЛКО :))
/*int get_index(struct _tuple_ *table, const char *kks)
{
	int index = 9000;
    const struct _tuple_ *note = pass_table(table, (void *)kks, &search_index);

    if (note != NULL)
    {
    	index = note->index;
    }

    return index;
    
}
*/
