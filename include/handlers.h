#ifndef _HANDLERS_H
#define _HANDLERS_H

#include "dts.h"
#include "json.h"

/*
 *	Macros difinition
 */

#define _SUCCESS	0
#define _FAILURE	-1

/*
 *	Type declaration
 */



/*
 *	Data declaration
 */	

/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif
int 	getfifo(char *path, FILE *data);
int 	parser(const char *msg, DTSClient& c, struct _config_ *);
void 	get_config(char *, struct _config_ *);
int 	get_quality(int );
int 	get_index(struct _tuple_ *, const char *);
char* 	get_kks(struct _tuple_ *, int *);

#ifdef __cplusplus
}
#endif

#endif /* _NAME_H */
